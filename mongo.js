// Mini-Activity

db.fruits.insertMany([
    {
        name: "Apple",
        supplier: "Red Farms Inc.",
        stocks: 20,
        price: 40,
        onSale: true
    },
    {
        name: "Banana",
        supplier: "Yellow Farms",
        stocks: 15,
        price: 20,
        onSale: true
    },
    {
        name: "Kiwi",
        supplier: "Green Farming and Canning",
        stocks: 25,
        price: 50,
        onSale: true
    },
    {
        name: "Mango",
        supplier: "Yellow Farms",
        stocks: 10,
        price: 60,
        onSale: true
    },
    {
        name: "Dragon Fruit",
        supplier: "Red Farms Inc.",
        stocks: 10,
        price: 60,
        onSale: true
    }
])

// Aggregation in MongoDB
    // process of generating manipulated data and perform operations to create filtered results that helps in data analysis
    // this helps in creating reports from analyzing data provided in our documents

    // Aggregation Pipelines
        // aggregation is done in 2 to 3 steps(every step is called pipeline)
        
        // first pipeline - $match - checks the documents that will pass the condition
            // Syntaxx: {$match:{existingField: value}}

        // second pipeline - $group - grouped documents together to create analysis of these grouped documents
            // Syntax: {$group: {_id: <id or existingField>, fieldResultName: "valueResult"}}

            // $sum - get the total of all values in the given field

    db.fruits.aggregate([
        {
            $match: {onSale: true}
        },
        {
            $group: {_id: "$supplier", totalStocks: {$sum: "$stocks"}}
        }
    ])

// Mini-Activity

db.fruits.aggregate([
    {
        $match: {supplier: "Red Farms Inc."}
    },
    {
        $group: {_id: "$supplier", totalStocks: {$sum: "$stocks"}}
    }
])


// average price of all items on sale

db.fruits.aggregate([
    {
        $match: {onSale: true}
    },
    {
        $group: {_id: "avgPriceOnSale", avgPrice: {$avg: "$price"}}
    }
])

// multiply operator

db.fruits.aggregate([
    {
        $match: {onSale: true}
    },
    {
        $project: {supplier: 1, _id: 0, inventory: {$multiply: ["$price", "$stocks"]}}
    }
])

// $max - gets the highest value out of all the values in the given field

db.fruits.aggregate([
    {
        $match: {onSale: true}
    },
    {
        $group: {_id: "maxStockOnSale", maxStock: {$max: "$stocks"}}
    }
])

// $min - gets the lowest value out of all the values in the given field

db.fruits.aggregate([
    {
        $match: {onSale: true}
    },
    {
        $group: {_id: "minStockOnSale", minStock: {$min: "$stocks"}}
    }
])

// $count - shows the total count of matched items

db.fruits.aggregate([
    {
        $match: {onSale: true}
    },
    {
        $count: "itemsOnSale"
    }
])

// count total number of items per group

db.fruits.aggregate([
    {
        $match: {onSale: true}
    },
    {
        $group: {_id: "$supplier", noOfItems: {$sum: 1}, totalStocks: {$sum: "$stocks"}}
    }
])

// creates another collection with the aggregated results

db.fruits.aggregate([
    {
        $match: {onSale: true}
    },
    {
        $group: {_id: "$supplier", totalStocks: {$sum: "$stocks"}}
    },
    {
        $out: "totalStocksPerSupplier"
    }
])